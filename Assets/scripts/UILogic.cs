﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UILogic : MonoBehaviour
{
    public AudioSource audioSource;
    public ObjectSpawner spawner;
    public GameObject pauseButton;
    public GameObject pausePanel;
    public GameObject resumeButton;
    public Text score;
    public Text time;

    public Image img_Timer;
    int countPoints=0;
    float countSeconds=0;
    Timer timer;


    public void addScore(int points)
    {
        countPoints += points;
        score.text = countPoints.ToString();
    }
    public void setTime(float seconds)
    {
        countSeconds = (int)seconds;
        img_Timer.fillAmount = countSeconds / 60;
        if (countSeconds < 0) 
            time.text = "";
        else time.text = countSeconds.ToString();
    }

    public void resume()
    {
        audioSource.Play();
        pauseButton.SetActive(true);
        pausePanel.SetActive(false);
        spawner.gameObject.SetActive(true);
        timer.resumeTimer();
    }
    public void pause()
    {
        timer.pauseTimer();
        audioSource.Pause();
        spawner.gameObject.SetActive(false);
        pauseButton.SetActive(false);
        pausePanel.SetActive(true);
    }
    public void gameOver()
    {
        pause();
        resumeButton.SetActive(false);
    }
    public void restart()
    {
        audioSource.Stop();
        audioSource.Play();
        resumeButton.SetActive(true);
        pausePanel.SetActive(false);
        pauseButton.SetActive(true);
        countPoints = 0;
        spawner.StartGame();
        spawner.gameObject.SetActive(true);
        timer.startTimer();
    }
    public void mainMenu()
    {
        audioSource.Stop();
        SceneManager.LoadScene(0);
    }


    void frameTimer()
    {
        setTime(timer.time);
    }
    void Start()
    {
        timer=gameObject.AddComponent<Timer>();
        ObjectSpawner.uILogic = this;
        timer.endAct = gameOver;
        timer.frameAct = frameTimer;
        restart();
    }
}
