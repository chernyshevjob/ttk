﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{

    static public AudioSource asrc;
    public AudioClip eazy;
    public AudioClip medium;
    public AudioClip hard;
    
    // Start is called before the first frame update
    void Start()
    {
        asrc = GetComponent<AudioSource>();
        asrc.Stop();
        switch (ObjectSpawner.gameDifficulty) {
            case 0:
                asrc.clip = eazy;
            break;
            case 1:
                asrc.clip = medium;
            break;
            case 3:
                asrc.clip = hard;
            break;
        }
        asrc.Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
