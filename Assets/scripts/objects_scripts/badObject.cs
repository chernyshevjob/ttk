﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class badObject : FlyingOobjectBehavior
{
    public float explosePower = 100;
    public float exploseRadius = 10;
    Vector2 exploseVector;
    public override void SendKick()
    {
        ObjectSpawner.BadAudio.Play();
        ObjectSpawner.Point.transform.position = transform.position;
        ObjectSpawner.Point.Play();

        foreach (RaycastHit2D h in Physics2D.CircleCastAll(transform.position, exploseRadius, Vector2.zero))
        {
            exploseVector = h.transform.position - transform.position;
          
                h.transform.GetComponent<Rigidbody2D>().
                AddForce(exploseVector.normalized* explosePower / (exploseVector.magnitude+0.1f), ForceMode2D.Impulse);
                //Debug.Log(explosePower / exploseVector.magnitude * (exploseVector.normalized));
        }
        ObjectSpawner.uILogic.addScore(-1);

        gameObject.SetActive(false);
    }

    public override void Spawn(Vector2 position, Vector2 velocity) {
        ObjectSpawner.Cloud.transform.position = position;
        ObjectSpawner.Cloud.Play();
        base.Spawn(position,velocity);
    }

    public override void Loss()
    {
        ObjectSpawner.uILogic.addScore(1);
        gameObject.SetActive(false);
    }

}
