﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Rigidbody2D))]
public class FlyingOobjectBehavior : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler
{
    //object component references
    //protected SpriteRenderer sr;
    //protected Sprite s = Sprite.Create(new Texture2D(100,100),new Rect(),new Vector2(0.5f,0.5f));
    protected Rigidbody2D rb;
    protected Vector2 velocity=Vector2.zero;


    public virtual void SendKick()
    {
        gameObject.SetActive(false);
    }

    public virtual void Spawn(Vector2 position,Vector2 velocity)
    {
        gameObject.SetActive(true);
        rb.velocity = velocity;
        rb.position = position;
    }

    public virtual void Loss()
    {
        gameObject.SetActive(false);
    }
    public FlyingOobjectBehavior()
    {
    }
    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        
        gameObject.SetActive(false);

    }
    void Update()
    {
        velocity = rb.velocity;
        if (transform.position.y < ObjectSpawner.lossBorder.yMin|| transform.position.y > ObjectSpawner.lossBorder.yMax || transform.position.x < ObjectSpawner.lossBorder.xMin || transform.position.x > ObjectSpawner.lossBorder.xMax )
            Loss();
        
    }

    void OnEnable()
    {
         rb.velocity = velocity;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        SendKick();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        SendKick();
    }
}
