﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class goodObject : FlyingOobjectBehavior
{
    //public 
    public override void SendKick()
    {    
        ObjectSpawner.GoodAudio.Play();
        ObjectSpawner.Stars.transform.position = transform.position;
        ObjectSpawner.Stars.Play();
        ObjectSpawner.uILogic.addScore(1);
        gameObject.SetActive(false);
    }
    public override void Spawn(Vector2 position, Vector2 velocity)
    {
        ObjectSpawner.Cloud.transform.position = position;
        ObjectSpawner.Cloud.Play();
        gameObject.SetActive(true);
        rb.velocity = velocity *0.7f;
        rb.position = position;
    }
    public override void Loss()
    {
        ObjectSpawner.uILogic.addScore(-1);
        gameObject.SetActive(false);
    }
}
