﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scaler : MonoBehaviour
{
#region public var
    [Range(0,10)]
    public float frequency = 1;

    [Range(0,1)]
    public float amplitude = 0.5f;
#endregion

#region private var
    private Vector3 scale;
#endregion
    // Start is called before the first frame update
    void Start()
    {
        scale = transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {
        // = ;
        transform.localScale = scale * (Mathf.Cos(frequency * Mathf.PI * Time.time) * amplitude + 1);
    }
}
