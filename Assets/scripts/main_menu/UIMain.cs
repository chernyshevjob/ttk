﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using UnityEngine.UI;

public class UIMain : MonoBehaviour
{
    public GameObject startButton;
    public Button eazyButton;
    public Button mediumButton;
    public Button hardButton;

    static bool isConnected = false;
    void Start()
    {
        ObjectSpawner.gameDifficulty = 0;
        if (!isConnected)
        {
            int m_ServerSocket;
            byte m_ChannelID;
            HostTopology m_HostTopology;
            //Set up the Connection Configuration which holds channel information
            ConnectionConfig config = new ConnectionConfig();
            //Add a reliable channel mode to the configuration (all messages delivered, not particularly in order)
            m_ChannelID = config.AddChannel(QosType.Reliable);
            //Create a new Host information based on the configuration created, and the maximum connections allowed (20)
            m_HostTopology = new HostTopology(config, 20);
            //Initialise the NetworkTransport
            NetworkTransport.Init();

            byte error;
            m_ServerSocket = NetworkTransport.AddHost(m_HostTopology, 54321);
            NetworkTransport.Connect(m_ServerSocket, "127.0.0.1", 54321, 0, out error);
        }
        else
            startButton.SetActive(true);

    
    }
    void Update()
    {
        if (!isConnected)
        {
            //These are the variables that are replaced by the incoming message
            int outHostId;
            int outConnectionId;
            int outChannelId;
            byte[] buffer = new byte[1024];
            int receivedSize;
            byte error;

            //Set up the Network Transport to receive the incoming message, and decide what type of event
            NetworkEventType eventType = NetworkTransport.Receive(out outHostId, out outConnectionId, out outChannelId, buffer, buffer.Length, out receivedSize, out error);

            if (eventType == NetworkEventType.ConnectEvent)
            {
                startButton.SetActive(true);
                isConnected = true;
            }
        }
    }
    public void startGame()
    {
        SceneManager.LoadScene(1);
    }
    public void eazy()
    {
        ObjectSpawner.gameDifficulty = 0;
        eazyButton.GetComponent<Image>().color = Color.green;
        mediumButton.GetComponent<Image>().color = Color.white;
        hardButton.GetComponent<Image>().color = Color.white;

    }

    public void medium()
    {
        ObjectSpawner.gameDifficulty = 1;
        eazyButton.GetComponent<Image>().color = Color.white;
        mediumButton.GetComponent<Image>().color = Color.yellow;
        hardButton.GetComponent<Image>().color = Color.white;

    }

    public void hard()
    {
        ObjectSpawner.gameDifficulty = 3;
        eazyButton.GetComponent<Image>().color = Color.white;
        mediumButton.GetComponent<Image>().color = Color.white;
        hardButton.GetComponent<Image>().color = Color.red;
    }

}
