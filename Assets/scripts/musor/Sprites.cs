﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

static public class Sprites 
{

    static public Dictionary<string,Sprite> allSprites;
    static public void LoadSprites()
    {
        LoadSprites("Sprites");
    }
    static public void LoadSprites(string path)
    {
        allSprites.Add("bad", Resources.Load<Sprite>(path+"/bad_sprite.png"));
        allSprites.Add("good", Resources.Load<Sprite>(path+"/good_sprite.png"));
    }
}
