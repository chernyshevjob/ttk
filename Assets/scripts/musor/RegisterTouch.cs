﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(KickObject))]
public class RegisterTouch : MonoBehaviour
{
    KickObject kick;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        foreach (Touch t in Input.touches)
        {
            kick.Kick(t.position);
        }
    }
}
