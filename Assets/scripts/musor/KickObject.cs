﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KickObject
{
    KickObject()
    {
        GameObject go = GameObject.Find("Main Camera");
        if (go != null)
            camera = go.GetComponent<Camera>();
        else camera = Camera.main;
    }

    public Camera camera;
    /// <summary>
    /// activates an object at a given screen coordinate
    /// </summary>
    public void Kick(int x,int y)
    {
        foreach (RaycastHit2D r in Physics2D.CircleCastAll(camera.ScreenToWorldPoint(new Vector3(x, y)), 1, Vector2.zero))
        {
           // r.transform.GetComponent<FlyingOobjectBehavior>().
        }
        
    }
    /// <summary>
    /// activates an object at a given screen coordinate
    /// </summary>
    public void Kick(Vector2 position)
    {
        Kick((int)position.x,(int)position.y);
    }
}
