﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public float endTime = 60;
    public delegate void timerAct();
    public timerAct endAct;
    public timerAct frameAct;
    public float time;

    bool isWork;
    public void startTimer()
    {
        time = 60;
        isWork = true;
    }
    public void pauseTimer()
    {
        isWork = false;
    }
    public void resumeTimer()
    {
        isWork = true;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }
    

    // Update is called once per frame
    void FixedUpdate()
    {
        if (isWork)
        {
            frameAct();
            if (time <= 0)
            {
                time = 60;
                isWork = false;
                endAct();
            }
            time -= Time.deltaTime;
        }

    }
}
