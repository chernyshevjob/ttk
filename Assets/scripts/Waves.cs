using UnityEngine;

public class Waves : MonoBehaviour {
    //public float scale = 1.0F;
    public Shader shader;
    private Renderer rend;

    void Start() {
        rend = GetComponent<Renderer>();
        rend.material.shader = shader;
        rend.material.SetTexture("_Noise1", CalcNoise(scale: 4f));
        rend.material.SetTexture("_Noise2", CalcNoise(scale: 5f, xOrg: 10f));
    }
    Texture2D CalcNoise(int pixWidth = 256,int pixHeight = 256,float scale = 1.0F , float xOrg = 0.0F,float yOrg = 0.0F) {
        Texture2D noiseTex = new Texture2D(pixWidth, pixHeight);
        Color[] pix = new Color[noiseTex.width * noiseTex.height];
        float y = 0.0F;
        while (y < noiseTex.height) {
            float x = 0.0F;
            while (x < noiseTex.width) {
                float xCoord = xOrg + x / noiseTex.width * scale;
                float yCoord = yOrg + y / noiseTex.height * scale;
                float sample = Mathf.PerlinNoise(xCoord, yCoord);
                pix[(int)y * noiseTex.width + (int)x] = new Color(sample, sample, sample);
                x++;
            }
            y++;
        }
        noiseTex.SetPixels(pix);
        noiseTex.Apply();

        return noiseTex;
    }
    void Update() {
        //CalcNoise();
    }
}