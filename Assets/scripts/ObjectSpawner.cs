﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    static public AudioSource BadAudio;
    static public AudioSource GoodAudio;
    static public ParticleSystem Stars;
    static public ParticleSystem Cloud;
    static public ParticleSystem Point;
    static public Rect lossBorder;
    static public UILogic uILogic;
    static public int gameDifficulty = 0;

    public float spawnInterval=0.5f;
    float nextSpawnDelay ;
    public float SprayThrow = 0.6f;
    //throw power
    public float minVelocity = 22f;
    public float maxVelocity = 44f;

    int maxObjectsValue = 0;
    public int maxObjects
    {
        get
        {
            return maxObjectsValue;
        }
        set
        {
            
            if (maxObjectsValue < value)
            {
                for (int j = 0; j < objectPrefab.Count; j++)
                {
                    flyingObjects.Add( new List<FlyingOobjectBehavior>());
                    for (int i = maxObjectsValue; i < value; i++)
                    {
 
                        flyingObjects[j].Add(Instantiate(objectPrefab[j]).GetComponent<FlyingOobjectBehavior>());
                        //set ObjectSpawner as parent for the realization of pauses
                        flyingObjects[j][i].transform.SetParent(transform);
                    }
                }
            }
            else
            {
                for (int j = 0; j < objectPrefab.Count; j++)
                    for (int i = maxObjectsValue; i > value; i--)
                    {
                        Destroy(flyingObjects[j][i].gameObject);
                        flyingObjects[j].Remove(flyingObjects[j][i]);
                    }
                for (int i = 0; i < objectPrefab.Count; i++)
                {
                    if (currentObjectForSpawn[i] > value)
                        currentObjectForSpawn[i] = 0;
                }
            }
            maxObjectsValue = value;
        }
    }  

    //Prefabs of objects and the chance of their appearance
    public List<GameObject> objectPrefab=new List<GameObject>();
    public List<float> objectChance=new List<float>();

    List<List<FlyingOobjectBehavior>> flyingObjects=new List<List<FlyingOobjectBehavior>>();
    List<int> currentObjectForSpawn=new List<int>();

    //objects will be thrown from the borders of this rect
    public Rect spawnBorder = new Rect(Vector2.zero, new Vector2(20, 20));
    //the vector of the impulse of a throw can be directed to any point around the center of the Rect spawnBorder.
    //This parameter (0 ... ~ 0.9) indicates how far from the center
    //the impulse vector direction point can be located.
    void setSprayThrow(float value)
    {

            sprayArea = new Rect(spawnBorder.min + (1 - value) / 2 * spawnBorder.max, value * spawnBorder.size);

    }
    Rect sprayArea;

    bool isGameStarted = false;
    //Timer timer = new Timer();

    void throwObject()
    {
        float randomFloat = Random.Range(0, objectChance[objectChance.Count - 1]);
        for (int i = 0; i < objectChance.Count; i++)
        {
            if (objectChance[i] >= randomFloat)
            {
                randomSpawn(i);
                currentObjectForSpawn[i]++;
                if (currentObjectForSpawn[i] > maxObjects-1)
                    currentObjectForSpawn[i] = 0;
                break;
            }
        }
    }
    void randomSpawn(int i)
    {
       //select random position
       Vector2 position=Vector2.zero;
        switch (Random.Range(0, 4))
        {
            
            case 0:
               
                position = new Vector2(Random.Range(0f, 1f) * spawnBorder.size.x, spawnBorder.min.y);
                break;

            case 1:
              
                position = new Vector2(Random.Range(0f, 1f) * spawnBorder.size.x, spawnBorder.max.y);
                break;

            case 2:
              
                position = new Vector2(spawnBorder.min.x, Random.Range(0f, 1f) * spawnBorder.size.y);
                break;

            case 3:
               
                position = new Vector2(spawnBorder.max.x, Random.Range(0f, 1f) * spawnBorder.size.y);
                break;
        }
        //select random direction
        Vector2 direction = new Vector2(sprayArea.position.x+Random.Range(0,sprayArea.width), sprayArea.position.y + Random.Range(0, sprayArea.height));
        //push position, select and push random velocity
        flyingObjects[i][currentObjectForSpawn[i]].Spawn(position, Random.Range(minVelocity, maxVelocity)*(direction-position).normalized);
       
    }
    
    public void StartGame()
    {
        foreach (List<FlyingOobjectBehavior> l in flyingObjects)
            foreach (FlyingOobjectBehavior f in l)
                f.gameObject.SetActive(false);
      //isGameStarted = true;
        Debug.Log("startg");
    
    }

    void Awake()
    {
        Stars = GameObject.Find("Stars").GetComponent<ParticleSystem>();
        Cloud = GameObject.Find("Cloud").GetComponent<ParticleSystem>();
        Point = GameObject.Find("Points").GetComponent<ParticleSystem>();

        BadAudio = GameObject.Find("Audio Source Object Bad").GetComponent<AudioSource>();
        GoodAudio = GameObject.Find("Audio Source Object Good").GetComponent<AudioSource>();

        spawnInterval *= 1f - 0.15f * gameDifficulty;
        maxVelocity *= 1f + 0.2f * gameDifficulty; 

        setSprayThrow (SprayThrow);

        //objectChance adjustment to simplify calculation
        for (int i = 1; i < objectChance.Count; i++)
            for (int j = 0; j < i; j++)
                objectChance[i] += objectChance[j];

        for (int i = 0; i < objectPrefab.Count; i++)
            currentObjectForSpawn.Add(0);

        maxObjects = 10;

        nextSpawnDelay = spawnInterval;

        Camera.main.transform.position =new Vector3( spawnBorder.center.x, spawnBorder.center.y,-10);
        lossBorder = new Rect(Camera.main.ScreenToWorldPoint(Vector3.zero), Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0))- Camera.main.ScreenToWorldPoint(Vector3.zero));
    }

    // Update is called once per frame
    void FixedUpdate()
    {
            nextSpawnDelay -= Time.deltaTime;
            if (nextSpawnDelay < 0)
            {
                nextSpawnDelay += spawnInterval;
                throwObject();

            }  
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        
        Gizmos.DrawLine(spawnBorder.min, spawnBorder.max);
       
        // Gizmos.DrawSphere(spawnBorder.max, 1);
    }
}
