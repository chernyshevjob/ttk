﻿Shader "Custom/Waves"
{
    Properties
    {
        _MainTex ("Main", 2D) = "white" {}
        _Color ("Main Color", Color) = (1,1,1,1)
        _Size ("Noise Size", float) = 1.0
        _ScrollX ("Scroll Speed X" , float) = 1
        _ScrollY ("Scroll Speed Y" , float) = 0.5
        _DeltaX ("Delta Scroll Speed X" , float) = 3
        _DeltaY ("Delta Scroll Speed Y" , float) = -2
    }
    SubShader
    {
        Tags {"Queue"="Transparent" "RenderType"="Transparent" }
        LOD 100

        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows alpha:fade

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
        };

        fixed4 _Color;

        float _Size;
        float _ScrollX;
        float _ScrollY;

        float _DeltaX;
        float _DeltaY;
        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

         float4 permute(float4 x)
            {
                return fmod(34.0 * pow(x, 2) + x, 289.0);
            }

            float2 fade(float2 t) {
                return 6.0 * pow(t, 5.0) - 15.0 * pow(t, 4.0) + 10.0 * pow(t, 3.0);
            }

            float4 taylorInvSqrt(float4 r) {
                return 1.79284291400159 - 0.85373472095314 * r;
            }

            #define DIV_289 0.00346020761245674740484429065744f

            float mod289(float x) {
                return x - floor(x * DIV_289) * 289.0;
            }

            float3 PerlinNoise2D(float2 P)
            {
                    float4 Pi = floor(P.xyxy) + float4(0.0, 0.0, 1.0, 1.0);
                    float4 Pf = frac (P.xyxy) - float4(0.0, 0.0, 1.0, 1.0);

                    float4 ix = Pi.xzxz;
                    float4 iy = Pi.yyww;
                    float4 fx = Pf.xzxz;
                    float4 fy = Pf.yyww;

                    float4 i = permute(permute(ix) + iy);

                    float4 gx = frac(i / 41.0) * 2.0 - 1.0 ;
                    float4 gy = abs(gx) - 0.5 ;
                    float4 tx = floor(gx + 0.5);
                    gx = gx - tx;

                    float2 g00 = float2(gx.x,gy.x);
                    float2 g10 = float2(gx.y,gy.y);
                    float2 g01 = float2(gx.z,gy.z);
                    float2 g11 = float2(gx.w,gy.w);

                    float4 norm = taylorInvSqrt(float4(dot(g00, g00), dot(g01, g01), dot(g10, g10), dot(g11, g11)));
                    g00 *= norm.x;
                    g01 *= norm.y;
                    g10 *= norm.z;
                    g11 *= norm.w;

                    float n00 = dot(g00, float2(fx.x, fy.x));
                    float n10 = dot(g10, float2(fx.y, fy.y));
                    float n01 = dot(g01, float2(fx.z, fy.z));
                    float n11 = dot(g11, float2(fx.w, fy.w));

                    float2 fade_xy = fade(Pf.xy);
                    float2 n_x = lerp(float2(n00, n01), float2(n10, n11), fade_xy.x);
                    float n_xy = lerp(n_x.x, n_x.y, fade_xy.y);

                float ns = 2.3 * n_xy / 2 + 0.5f;

                    return float3(ns,ns,ns);
            }


        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            fixed2 scrolledUVp = IN.uv_MainTex * _Size;
            fixed xScrollValue = _ScrollX * _Time;
            fixed yScrollValue = _ScrollY * _Time;
            scrolledUVp += float2(xScrollValue, yScrollValue);

            fixed2 scrolledUVn = IN.uv_MainTex * _Size;
            scrolledUVn += float2(xScrollValue * _DeltaX, yScrollValue * _DeltaY);

            float3 perlinP = PerlinNoise2D(scrolledUVp);
            float3 perlinN = PerlinNoise2D(scrolledUVn);

            o.Albedo = clamp(perlinP * perlinN * 2.3f, 0.5f,1) * _Color;
            o.Alpha = tex2D (_MainTex, IN.uv_MainTex).a;
        }

       
        ENDCG
    }
    FallBack "Diffuse"
}